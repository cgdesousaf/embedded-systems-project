#include "state.h"

static state_t current_state = STEP_1;

mstate_t state_machine[] = {
    { .action=display_step},
    { .action=accelerometer_step},
    { .action=compass_step},
    { .action=thermometer_step},
    { .action=bluetooth_step},
};

state_t state_machine_get_state()
{
    return current_state;
}

void state_machine_set_state(state_t state)
{
    current_state = state;
}

void state_machine_next()
{
    struct mb_display *display = mb_display_get();
    mb_display_stop(display);

    current_state++;
    if(current_state > NUM_STATES - 1) {
        current_state = 0;
    }
}

void state_machine_prev()
{
    struct mb_display *display = mb_display_get();
    mb_display_stop(display);

    if(current_state == 0) {
        current_state = NUM_STATES - 1;
    } else {
        current_state--;
    }
}

void state_machine_execute()
{   
    state_machine[current_state].action();
}
