#include "compass.h"

static struct i2c_dev compass;
static struct compass_sample sample;
static struct compass_sample average;

void compass_configure(void)
{

    i2c_util_dev_init(&compass, MAG3110_DEFAULT_ADDR, "COMPASS",
                            MAG_WHOAMI, MAG_TEST_VALUE);

    uint8_t temp = 0x00;
    // First, take the device offline, so it can be configured.
    i2c_util_write_bytes(&compass, MAG_CTRL_REG1, &temp, sizeof(uint8_t));

    // Enable automatic reset after each sample;
    temp = 0b10000000;;
    i2c_util_write_bytes(&compass, MAG_CTRL_REG2, &temp, sizeof(uint8_t));

    // Bring the device online, with the requested sample frequency.
    temp = 0x01;
    i2c_util_write_bytes(&compass, MAG_CTRL_REG1, &temp, sizeof(uint8_t));

    uint8_t mode;
    i2c_util_read_bytes(&compass, MAG_SYSMOD, &mode, sizeof(uint8_t));
    SYS_LOG_DBG("Compass Mode:%d", mode);

}

void compass_calibrate()
{
    average.x = 10000;
    average.y = 10000;
    average.z = 10000;
}

void compass_update_sample()
{
    i2c_util_read_bytes(&compass, MAG_OUT_X_MSB, &sample.x, 2);
    i2c_util_read_bytes(&compass, MAG_OUT_Y_MSB, &sample.y, 2);
    i2c_util_read_bytes(&compass, MAG_OUT_Z_MSB, &sample.z, 2);

    sample.x = MAG3110_NORMALIZE_SAMPLE(sample.x);
    sample.y = MAG3110_NORMALIZE_SAMPLE(sample.y);
    sample.z = MAG3110_NORMALIZE_SAMPLE(sample.z);
}

int compass_get_X(coordinate_system system)
{
    compass_update_sample();

    switch (system)
    {
        case SIMPLE_CARTESIAN:
            return sample.x - average.x;

        case NORTH_EAST_DOWN:
            return -(sample.y - average.y);

        case RAW:
        default:
            return sample.x;
    }
}

int compass_get_Y(coordinate_system system)
{
    compass_update_sample();

    switch (system)
    {
        case SIMPLE_CARTESIAN:
            return -(sample.y - average.y);

        case NORTH_EAST_DOWN:
            return (sample.x - average.x);

        case RAW:
        default:
            return sample.y;
    }
}

int compass_get_Z(coordinate_system system)
{
    compass_update_sample();

    switch (system)
    {
        case SIMPLE_CARTESIAN:
        case NORTH_EAST_DOWN:
            return -(sample.z - average.z);

        case RAW:
        default:
            return sample.z;
    }
}

int compass_tilt_compensated_bearing()
{
    // Precompute the tilt compensation parameters to improve readability.
    float phi = accelerometer_get_roll_radians();
    float theta = accelerometer_get_pitch_radians();

    float x = (float) compass_get_X(NORTH_EAST_DOWN);
    float y = (float) compass_get_Y(NORTH_EAST_DOWN);
    float z = (float) compass_get_Z(NORTH_EAST_DOWN);

    // Precompute cos and sin of pitch and roll angles to make the calculation a little more efficient.
    float sinPhi = sin(phi);
    float cosPhi = cos(phi);
    float sinTheta = sin(theta);
    float cosTheta = cos(theta);

    float bearing = (360*atan2(z*sinPhi - y*cosPhi, x*cosTheta + y*sinTheta*sinPhi + z*sinTheta*cosPhi)) / (2*PI);

    if (bearing < 0)
        bearing += 360.0;

    return (int) bearing;
}

int compass_basic_bearing()
{
    compass_update_sample();

    float bearing = (atan2((double)(sample.y - average.y),(double)(sample.x - average.x)))*180/PI;

    if (bearing < 0)
        bearing += 360.0;

    return (int)(360.0 - bearing);
}

int compass_heading()
{
    compass_calibrate();

    return compass_tilt_compensated_bearing();
}
