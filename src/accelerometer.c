#include "accelerometer.h"


static struct i2c_dev acc; /*!< Accelerometer I2C device */

void accelerometer_configure(void)
{
    i2c_util_dev_init(&acc, MMA8653_DEFAULT_ADDR, "ACC", MMA8653_WHOAMI_VAL, MMA8653_TEST_VALUE);

    uint8_t temp = 0x00;

    // Bring accelerometer to Standby Mode
    i2c_util_write_bytes(&acc, MMA8653_CTRL_REG1, &temp, sizeof(uint8_t));

    // Enable high precision mode. This consumes a bit more power, but still only 184 uA!
    temp = 0x10;
    i2c_util_write_bytes(&acc, MMA8653_CTRL_REG2, &temp, sizeof(uint8_t));

    // Enable the INT1 interrupt pin.
    temp = 0x01;
    i2c_util_write_bytes(&acc, MMA8653_CTRL_REG4, &temp, sizeof(uint8_t));

    // Select the DATA_READY event source to be routed to INT1
    temp = 0x01;
    i2c_util_write_bytes(&acc, MMA8653_CTRL_REG1, &temp, sizeof(uint8_t));

    // Configure for the selected g range.
    temp = 0x0E;
    i2c_util_write_bytes(&acc, MMA8653_XYZ_DATA_CFG, &temp, sizeof(uint8_t));

    // Bring accelerometer to Active Mode
    temp = 0x01;
    i2c_util_write_bytes(&acc, MMA8653_CTRL_REG1, &temp, sizeof(uint8_t));

    uint8_t mode;
    i2c_util_read_bytes(&acc, MMA8653_OUT_X_MSB, &mode, sizeof(uint8_t));
    SYS_LOG_DBG("Acc Mode:%d", mode);
}

static struct acc_sample sample; /*! Accelerometer sample XYZ */

void accelerometer_update_sample()
/*! < This function updates the sample read from the accelerometer,
    it must be called every time we need to check on this sensor data. */
{
    int8_t data[6];

    i2c_util_read_bytes(&acc, 0x01, &data, 6);

    sample.x = data[0];
    sample.y = data[2];
    sample.z = data[4];
}

int16_t accelerometer_get_X(coordinate_system system)
{
    accelerometer_update_sample();

    switch (system)
    {
        case SIMPLE_CARTESIAN:
            return -sample.x;

        case NORTH_EAST_DOWN:
            return sample.y;

        case RAW:
        default:
            return sample.x;
    }

}

int16_t accelerometer_get_Y(coordinate_system system)
{
    accelerometer_update_sample();

    switch (system)
    {
        case SIMPLE_CARTESIAN:
            return -sample.y;

        case NORTH_EAST_DOWN:
            return -sample.x;

        case RAW:
        default:
            return sample.y;
    }
}

int16_t accelerometer_get_Z(coordinate_system system)
{
    accelerometer_update_sample();

    switch (system)
    {
        case NORTH_EAST_DOWN:
            return -sample.z;

        case SIMPLE_CARTESIAN:
        case RAW:
        default:
            return sample.z;
    }
}

static float roll; /*! Roll angles in degrees */
static float pitch; /*! Pitch angles in degrees */

void accelerometer_recalc_roll_radians()
{
    double x = (double) accelerometer_get_X(NORTH_EAST_DOWN);
    double y = (double) accelerometer_get_Y(NORTH_EAST_DOWN);
    double z = (double) accelerometer_get_Z(NORTH_EAST_DOWN);

    roll = atan2(y, z);
    pitch = atan(-x / (y*sin(roll) + z*cos(roll)));
}

float accelerometer_get_roll_radians()
{
    accelerometer_recalc_roll_radians();
    return roll;
}

float accelerometer_get_pitch_radians()
{
    accelerometer_recalc_roll_radians();
    return pitch;
}
