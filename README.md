# embedded systems 2017.2 final project

This project is developed using BBC:microbit and Zephyr RTOS. 

It must implements a state machine with 5 different states: 

* State 01: It shows the text "ECOM042.2017.2" at microbit's display.
* State 02: It uses the accelerometer to move around a single pixel on its LED display. 
* State 03: It creates a compass functionality using accelerometer and magnetometer to show the magnetic north heading direction on its display.
* State 04: It measures the temperature and it shows on its display. 
* State 05: It uses Microbit's Bluetooth to communicate with an external device, receiving and handling data accordingly. 

These states were implemented using a proper state machine and Zephyr available libraries. 

To run this code, you need configurate Zephyr SDK for Microbit. 
