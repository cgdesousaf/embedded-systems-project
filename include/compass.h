#ifndef COMPASS_H
#define COMPASS_H

#include <zephyr.h>

#include "accelerometer.h"
#include "i2c_util.h"

/**
  * I2C constants
  */
#define MAG3110_DEFAULT_ADDR    0x0e

/**
  * MAG3110 Register map
  */

#define MAG_DR_STATUS 0x00
#define MAG_OUT_X_MSB 0x01
#define MAG_OUT_X_LSB 0x02
#define MAG_OUT_Y_MSB 0x03
#define MAG_OUT_Y_LSB 0x04
#define MAG_OUT_Z_MSB 0x05
#define MAG_OUT_Z_LSB 0x06
#define MAG_WHOAMI    0x07
#define MAG_SYSMOD    0x08
#define MAG_OFF_X_MSB 0x09
#define MAG_OFF_X_LSB 0x0A
#define MAG_OFF_Y_MSB 0x0B
#define MAG_OFF_Y_LSB 0x0C
#define MAG_OFF_Z_MSB 0x0D
#define MAG_OFF_Z_LSB 0x0E
#define MAG_DIE_TEMP  0x0F
#define MAG_CTRL_REG1 0x10
#define MAG_CTRL_REG2 0x11

#define MAG_TEST_VALUE   0xC4

#define PI 3.14159265

/**
  * Term to convert sample data into SI units
  */
#define MAG3110_NORMALIZE_SAMPLE(x) (100*x)

/**
  * Status Bits
  */
#define MICROBIT_COMPASS_STATUS_CALIBRATED      2
#define MICROBIT_COMPASS_STATUS_CALIBRATING     4
#define MICROBIT_COMPASS_STATUS_ADDED_TO_IDLE   8

struct compass_sample
{
    int16_t         x;
    int16_t         y;
    int16_t         z;
};

void compass_configure();
void compass_calibrate();
void compass_update_sample();
int  compass_get_X(coordinate_system system);
int  compass_get_Y(coordinate_system system);
int  compass_get_Z(coordinate_system system);
int  compass_heading();
int  compass_basic_bearing();
int  compass_tilt_compensated_bearing();

#endif
