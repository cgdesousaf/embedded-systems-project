/*! \headerfile accelerometer.h <accelerometer.h>
 *  \brief Accelerometer MMA8653 lib implementation.
 *         Port MICROBIT accelerometer to Zephyr.
 *
 *  It implements the accelerometer basic functionalities to work properly with
 *  Zephyr, using I2C to communicate with it.
 */

#ifndef ACCELEROMETER_H
#define ACCELEROMETER_H

#include <zephyr.h>
#include <math.h>

#include "i2c_util.h"

/**
  * I2C constants
  */
#define MMA8653_DEFAULT_ADDR    0x1D

/**
  * MMA8653 Register map (partial)
  */

#define MMA8653_STATUS          0x00
#define MMA8653_OUT_X_MSB       0x01
#define MMA8653_WHOAMI          0x0D
#define MMA8653_XYZ_DATA_CFG    0x0E
#define MMA8653_CTRL_REG1       0x2A
#define MMA8653_CTRL_REG2       0x2B
#define MMA8653_CTRL_REG3       0x2C
#define MMA8653_CTRL_REG4       0x2D
#define MMA8653_CTRL_REG5       0x2E

/**
  * MMA8653 constants
  */

#define MMA8653_WHOAMI_VAL      0x5A
#define MMA8653_TEST_VALUE      0x5A

typedef enum
{
    RAW,
    SIMPLE_CARTESIAN,
    NORTH_EAST_DOWN
} coordinate_system;

struct acc_sample
{
    int16_t         x;
    int16_t         y;
    int16_t         z;
};

void accelerometer_configure();
void accelerometer_update_sample();
int16_t accelerometer_get_X(coordinate_system system);
int16_t accelerometer_get_Y(coordinate_system system);
int16_t accelerometer_get_Z(coordinate_system system);
void accelerometer_recalc_roll_radians();
float accelerometer_get_roll_radians();
float accelerometer_get_pitch_radians();

#endif // STATE_H
