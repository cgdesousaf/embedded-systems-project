#ifndef DISPLAY_H
#define DISPLAY_H

#include <zephyr.h>
#include <display/mb_display.h>

void display_set_pixel(uint8_t x, uint8_t y);
void display_get_pixel(uint8_t x, uint8_t y);
void display_clear_pixel(uint8_t x, uint8_t y);
void display_reset();

#endif // STATE_H
